(require 'company)

(defconst dockerfile-completions '("FROM" "RUN" "CMD" "LABEL" "MAINTAINER" "EXPOSE" "ENV" "ADD" "COPY" "ENTRYPOINT" "VOLUME" "USER" "WORKDIR" "ARG" "ONBUILD" "STOPSIGNAL" "HEALTHCHECK" "SHELL"))

(defun company-dockerfile-backend (command &optional arg &rest ignore)
  (interactive (list 'interactive))

  (case command
	(interactive (company-begin-backend 'company-dockerfile-backend))
	(prefix (and (eq major-mode 'dockerfile-mode) (company-grab-symbol)))
	(candidates
	 (cl-remove-if-not
	  (lambda (c) (string-prefix-p arg c t))
	  dockerfile-completions))))

(add-to-list 'company-backends 'company-dockerfile-backend)
